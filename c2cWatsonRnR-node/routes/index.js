var path = require('path');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var twilio = require('twilio');
var config = require("../config");

var https = require('https'); // for request to Watson RnR

// Create a Twilio REST API client for authenticated requests to Twilio
var client = twilio(config.accountSid, config.authToken);

// Configure application routes
module.exports = function(app) {
    // Set Jade as the default template engine
    app.set('view engine', 'jade');

    // Express static file middleware - serves up JS, CSS, and images from the
    // "public" directory where we started our webapp process
    app.use(express.static(path.join(process.cwd(), 'public')));

    // Parse incoming request bodies as form-encoded
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // Use morgan for HTTP request logging
    app.use(morgan('combined'));

    // Home Page with Click to Call 
    app.get('/', function(request, response) {
        response.render('index');
    });

    // Handle an AJAX POST request to place an outbound call
    app.post('/call', function(request, response) {
        
        // This should be the publicly accessible URL for your application
        // Here, we just use the host for the application making the request,
        // but you can hard code it or use something different if need be
        
        sQueryString = encodeURI(request.body.question);
        
        console.log(" ... escaping question: " +   sQueryString);
        
        var url = 'http://' + request.headers.host + '/outbound?q=' +  sQueryString;
        
        // Place an outbound call to the user, using the TwiML instructions
        // from the /outbound route
        client.makeCall({
            to: request.body.phoneNumber,
            from: config.twilioNumber,
            url: url    
        }, function(err, message) {
            console.log(err);
            if (err) {
                response.status(500).send(err);
            } else {
                response.send({
                    message: 'Thank you! We will be calling you shortly.'
                });
            }
        });
    });

    // Return TwiML instuctions for the outbound call
    app.post('/outbound', function(request, response) {
        
        // Build query string for Watson RnR.
        var oQueryParams = {
            ranker_id : '3b140ax14-rank-684',
            q :  request.query.q,
            wt : 'json',
            fl :'id,title'
        }
        
        var sRnRQuery = '?ranker_id=' +  oQueryParams.ranker_id 
                  + '&q=' +  oQueryParams.q
                  + '&wt=' + oQueryParams.wt
                  + '&fl=' +  oQueryParams.fl;
        
        // HTTP Request Options
        var oRnROpts = {
            host: 'gateway.watsonplatform.net',
            path: encodeURI('/retrieve-and-rank/api/v1/solr_clusters/sc19a5c2f4_0f19_44d4_8272_c9901a1fe2bf/solr/example_collection/fcselect' + sRnRQuery),
            auth: '41c471f2-e413-4039-a7e4-1606e74686e0:XCBgdLZmVSaS'
        };
        
        var req = https.request(oRnROpts , function (oRnRresp) {
            var sAns = '';
            
            oRnRresp.on('data', function (chunk) {
                sAns += chunk;
            });
            
            oRnRresp.on('end', function () {
                
                // Get the top result from retrieve and rank
                var sAnswer = JSON.stringify(JSON.parse(sAns).response.docs[0].title[0]);
                
                // Render a TwiML (XML) response using Jade
                response.type('text/xml');
                response.render('outbound',
                                {answer: sAnswer});
            });
        });
        
        req.end();   
    });
    
    app.post('/handle_transcribe', function(request, response) {
      console.log(request.body);
      
    })
};


