/**
 * Created by Thomas on 6/5/2016.
 */
'use strict';

var path = require('path');
var express = require('express');
var morgan = require('morgan');
var sms = require('./smsServer');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(morgan('combined'));    // logging
app.use('/api/sms', sms);

var port = process.env.PORT || process.env.VCAP_APP_PORT || 3000;

app.listen(port, function () {
    console.log('Server running on port: %d', port);
});

