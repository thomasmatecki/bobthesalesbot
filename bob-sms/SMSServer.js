/**
 * Created by Thomas on 6/5/2016.
 */
'use strict';

var router = require('express').Router();
var twilio = require('twilio');
var nlc = require('./nlcClient');
//var db = require('./dbClient');

router.get('/', function (req, res) {

    nlc.queryClassifier(JSON.stringify(req.query.Body), function (err, results) {
        console.log(JSON.stringify(results));

        if (err) return next(err);
        else res.render('SMSResponse', {reply: results.top_class});
    });
});

module.exports = router;