/**
 * Created by Thomas on 6/8/2016.
 */

'use strict';


var watson = require('watson-developer-cloud');

var oClassifier = watson.natural_language_classifier({
    url: 'https://gateway.watsonplatform.net/natural-language-classifier/api',
    username: 'a648e118-b1e1-4fa0-befa-f0ec3333d80e',
    password: 'AEG5M4fiGLhT',
    version: 'v1'

});

module.exports.queryClassifier = function (sRequestText, callback) {

    var params = {
        classifier: process.env.CLASSIFIER_ID || '3a84d1x62-nlc-20441', // pre-trained classifier
        text: sRequestText
    };

    oClassifier.classify(params, callback);

};