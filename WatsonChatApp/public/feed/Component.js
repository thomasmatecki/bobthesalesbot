﻿sap.ui.define(['sap/ui/core/UIComponent'],
	function (UIComponent) {
    "use strict";
    var Component = UIComponent.extend("bob.test.m.Feed.Component", {
        metadata : {
            rootView : "bob.test.m.Feed.Main",
            dependencies : {
                libs : [
                    "sap.m"
                ]
            },
            config : {
                sample : {
                    files : [
                        "style.css"
                    ]
                }
            }
        }
    });
    return Component;
});
