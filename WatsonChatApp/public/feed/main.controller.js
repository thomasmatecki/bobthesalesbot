﻿sap.ui.define([
    'jquery.sap.global',
    'sap/m/MessageToast',
    'sap/ui/core/format/DateFormat',
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel'
], function (jQuery, MessageToast, DateFormat, Controller, JSONModel) {
    "use strict";
    
    var CController = Controller.extend("bob.test.m.Feed.main", {
        
        onInit: function () {
            // set mock model
            var sPath = jQuery.sap.getModulePath("bob.test.m.Feed", "/data.json")
            var oModel = new JSONModel(sPath);
            this.getView().setModel(oModel);
        },
        
        onPost: function (oEvent) {
            var oFormat = DateFormat.getDateTimeInstance({ style: "medium" });
            var oDate = new Date();
            var sDate = oFormat.format(oDate);
            // create new entry
            var sValue = oEvent.getParameter("value");
            
            var oEntry = {
                Author : "User",
                Type : "Question",
                Date : "" + sDate,
                Text : sValue
            };
            
            // update model
            var oModel = this.getView().getModel();
            var aEntries = oModel.getData().EntryCollection;
            aEntries.unshift(oEntry);
            oModel.setData({
                EntryCollection : aEntries
            });
            
            
            jQuery.ajax({
                url: '/question',
                type: 'POST',
                dataType: 'json',
                async : true,
                data: {
                    question : sValue
                },
                success : function (data, textStatus, jqXHR) {
                    //oModel.setData({ modelData : data });
                    console.log('post success')
                },
                error: function (jqXHR , textStatus, errorThrown) { },
                complete: function (jqXHR, textStatus) { }
            });
        },
        
        onSenderPress: function (oEvent) {
            MessageToast.show("Clicked on Link: " + oEvent.getSource().getSender());
        },
        
        onIconPress: function (oEvent) {
            MessageToast.show("Clicked on Image: " + oEvent.getSource().getSender());
        }
    });
    return CController;

});
