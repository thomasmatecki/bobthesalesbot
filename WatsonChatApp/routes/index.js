﻿var express = require('express');
var watsonClient = require('./watsonClient');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {
        title: 'Chat Feed',
        bootStrapURL: 'https://sapui5.netweaver.ondemand.com/resources/sap-ui-core.js',
        fqAppName : "'bob.test.m.Feed'",
        resourceRoots: '{"bob.test.m.Feed": "./feed"}'
    });
});

router.get('/feed/Main.view.xml', function (req, res) {
    res.render('main');
});

router.post('/question', function (req, res) {
    console.log("Received Question: " + req.body.question);
    
/* Call watson here.*/

});

module.exports = router;