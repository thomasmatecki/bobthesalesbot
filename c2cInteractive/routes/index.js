﻿"use strict";

var express = require('express');
var router = express.Router();
var twilio = require('twilio');
var config = require("../config");
var watson = require('watson-developer-cloud');
var https = require('https');
var qs = require('qs');

var twilioClient = twilio(config.twilio.account_SID, config.twilio.auth_token);
var speech_to_text = watson.speech_to_text(config.watson.speech_to_text);
var retrieve_and_rank = watson.retrieve_and_rank(config.watson.retrieve_and_rank);

var re
/* Home Page. */
router.get('/', function (request, response) {
    response.render('StartPage');
});


/* Call twilio API to initiate Call */
router.post('/StartCall', function (request, response) {
    
    console.log("[POST-REQ]StartCall... ");
    
    twilioClient.makeCall({
        
        to: request.body.phoneNumber,
        from: config.twilio.phone_number,
        url: 'http://' + request.headers.host + '/CallGreeting'

    }, function (err, message) {
        
        console.log('Error : ' + JSON.stringify(err));
        if (err) {
            response.status(500).send(err);
        } else {
            console.log('...Success');
            response.send({
                message: 'Calling'
            });
        }
    });
});

/* Inbound From twilio; Greet and capture first question */
router.post('/CallGreeting', function (request, response) {
    console.log('[POST-REQ]CallGreeting : ' + JSON.stringify(request.body));
    
    response.type('text/xml');
    response.render('SayThenRecord', { prompt: 'Hello. A little bird told me you were interested in code academy. Do you have any questions?' });
});

/* Inbound From twilio; When Prompted with a question: 
 *  (1) Pass URL of recorded question to watson Speech to text -> transcription of question.
 *  (2) Pass transcription to watson retrieve and rank.
 *  (3) Render top result from retrieve and rank back to twilio.
 */
router.post('/AnswerAndPrompt', function (request, response) {
    
    console.log('[POST-REQ]AnswerAndPrompt; Recording URL: ' + JSON.stringify(request.body.RecordingUrl));
    
    https.request(request.body.RecordingUrl, function (oRecordingResp) {
        speech_to_text.recognize({
            audio: oRecordingResp,
            content_type: 'audio/wav',
            model: 'en-US_NarrowbandModel'
        }, function (err, transcript) {
            
            if (err || transcript.results.length === 0) {
                console.log(err);
                response.type('text/xml');
                response.render('SayThenRecord', 
                                        { prompt: "Sorry, I didn't understand that. Can you say it again?" });

            } else {
                console.log('[WT2S-RES]AnswerAndPrompt :' + JSON.stringify(transcript));
                
                var solrClient = retrieve_and_rank.createSolrClient(config.watson.solr_params);
                
                solrClient.get('fcselect', qs.stringify({
                    q: 'q=' + JSON.stringify(transcript.results[0].alternatives[0].transcript), 
                    ranker_id: config.watson.retrieve_and_rank.ranker_id,
                    fl: 'id,title'
                }), function (err, searchResponse) {
                    if (err || searchResponse.response.docs === 0) {
                        console.log('Error searching for documents: ' + err);
                        response.type('text/xml');
                        response.render('SayThenRecord', 
                                        { prompt: "I don't see how that is relevant. Please only ask questions about code academy" });
                    } else {
                        console.log('[WRAR-RES]AnswerAndPrompt : ' + JSON.stringify(searchResponse.response.docs[0].title[0]));
                        response.type('text/xml');
                        response.render('SayThenRecord', 
                                        { prompt: JSON.stringify(searchResponse.response.docs[0].title[0]) });
                    }
                });
            }
        });
    }).end();
});

module.exports = router;