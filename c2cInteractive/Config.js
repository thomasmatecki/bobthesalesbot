﻿module.exports = {
    
    // A Twilio number, Account SID, and Auth Token
    twilio : {
        phone_number: '+12409496580',
        auth_token: '5a6d5eb79f4828254b003b3accec4a81',
        account_SID: 'ACde48c7317e00209f79415606e7b031db',
        api_host: 'api.twilio.com',
        api_path_prefix: '/2010-04-01/Accounts/'
    },
    
    watson: {
        speech_to_text: {
            password: "sByPzpWTeYcW",
            username: "024c0faf-465b-42de-a270-44c8166e7fa5",
            version: 'v1'
        },
        retrieve_and_rank : {
            username: '96bc12f8-4067-4717-9a7b-fa194a9461c2',
            password: '0APmdZJykheV',
            version: 'v1',
            ranker_id : '3b140ax14-rank-1331'
        },
        solr_params: {
            cluster_id: 'scaa5b4cdc_29f2_4110_9411_04f6e9577336',
            collection_name: 'example_collection',
        }

    },
    
    port: process.env.PORT || 3000
};